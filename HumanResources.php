<?php

class HumanResources
{
    /**
     * @var Employee[]
     */
    protected $employees = [];

    /**
     * @return Employee[]
     */
    public function getEmployees(): array
    {
        return $this->employees;
    }

    /**
     * @param Employee $employee
     * @return HumanResources
     */
    public function addEmployees(Employee $employee): HumanResources
    {
        if (! in_array($employee, $this->employees)) {
            $this->employees[] = $employee;
        }
        return $this;
    }

    /**
     * @param Employee $oldEmployee
     * @return HumanResources
     */
    public function removeEmployees(Employee &$oldEmployee): HumanResources
    {
        foreach ($this->employees as $key => $employee) {
            if ($employee === $oldEmployee) {
                unset($this->employees[$key]);
                break;
            }
        }
        return $this;
    }

    /**
     * @param int $id
     * @param null|string $name
     * @param null|string $lastname
     * @return HumanResources
     */
    public function updateEmployees(int $id, $name = null, $lastname = null): HumanResources
    {
        $employee = $this->getEmployee($id);
        if ($name !== null) {
            $employee->setName($name);
        }
        if ($lastname !== null) {
            $employee->setLastname($lastname);
        }
        return $this;
    }

    /**
     * @param int $id
     * @return Employee|bool
     */
    public function getEmployee(int $id): Employee
    {
        foreach ($this->employees as $employee) {
            if ($employee->getId() === $id) {
                return $employee;
            }
        }
        return false;
    }

    public function listEmployees()
    {
        foreach ($this->employees as $employee) {
            echo $employee->getNameAndId(). '<br />';
        }
    }
}