<?php

class Employee
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $lastname;

    public function __construct(int $id, string $name, string $lastname)
    {
        $this->id = $id;
        $this->name = $name;
        $this->lastname = $lastname;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Employee
     */
    public function setId(int $id): Employee
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Employee
     */
    public function setName(string $name): Employee
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastname(): string
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     * @return Employee
     */
    public function setLastname(string $lastname): Employee
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * @return string
     */
    public function getNameAndId(): string
    {
        return $this->getName() . ' ' . $this->getLastname() . ' [' . $this->getId() . ']';
    }
}