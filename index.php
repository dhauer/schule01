<?php

/**
 * Adding needed classes to this file
 */

require 'Employee.php';
require 'HumanResources.php';

/**
 * Employees
 */

$employeeArray = [
    [1, 'Dustin', 'Hauer'],
    [2, 'Manfred', 'Moppel'],
    [5, 'Jerald', 'Gunther'],
    [8, 'Ditmar', 'Alt'],
    [16, 'Pascal', 'Ritzenfeld'],
    [23, 'Sebastian', 'Frommel'],
    [56, 'Anna', 'Quotès'],
];

/**
 * Adding employees to the system
 */

$humanResources = new HumanResources();

foreach ($employeeArray as $employee) {
    $humanResources->addEmployees(new Employee($employee[0], $employee[1], $employee[2]));
}

/**
 * Showcase of the methods
 */

?>

<h1>Erstellt von Dustin Hauer und Pascal Ritzenfeld</h1>
<h2>Liste-Mitabeiter</h2>

<?php $humanResources->listEmployees(); ?>

<hr />
<h2>Mitarbeiter hinzufügen</h2>

<?php
    $e = new Employee(87, 'Manuel', 'Neuer');
    $humanResources->addEmployees($e);
    $humanResources->listEmployees();
?>

<hr />
<h2>Mitarbeiter auswählen</h2>

<?php
echo $humanResources->getEmployee(1)->getNameAndId();
?>

<hr />
<h2>Mitarbeiter updated</h2>

<?php
    $humanResources->updateEmployees(87, 'Gey');
    $humanResources->listEmployees();
?>

<hr />
<h2>Mitarbeiter entfernen</h2>

<?php
    $e = $humanResources->getEmployee(87);
    $humanResources->removeEmployees($e);
    $humanResources->listEmployees();
?>
<hr />
